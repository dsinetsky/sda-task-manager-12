package ru.t1.dsinetsky.tm.enumerated;

public enum Status {

    NOT_STARTED("Not started"),
    IN_PROGRESS("In progress"),
    COMPLETED("Completed");

    private final String displayName;

    Status(final String displayName) {
        this.displayName = displayName;
    }

    public static String toName(Status status) {
        if (status == null) return "";
        return status.getDisplayName();
    }

    public static String getStatuses() {
        String statusName = "";
        for (Status sts : values())
            statusName += "- " + sts.name() + "\n";
        return statusName;
    }

    public static Status toStatus(String status) {
        if (status == null || status.isEmpty()) return null;
        for (Status sts : values())
            if (status.equals(sts.name())) return sts;
        return null;
    }

    public String getDisplayName() {
        return displayName;
    }

}
