package ru.t1.dsinetsky.tm.api.service;

import ru.t1.dsinetsky.tm.enumerated.Status;
import ru.t1.dsinetsky.tm.model.Task;

import java.util.List;

public interface ITaskService {
    void clearAll();

    List<Task> returnAll();

    Task create(String name);

    Task create(String name, String desc);

    Task findById(String id);

    Task findByIndex(int index);

    Task removeById(String id);

    Task removeByIndex(int index);

    Task updateById(String id, String name, String desc);

    Task updateByIndex(int index, String name, String desc);

    Task changeStatusById(String id, Status status);

    Task changeStatusByIndex(int index, Status status);

    void createTestTasks();

}
