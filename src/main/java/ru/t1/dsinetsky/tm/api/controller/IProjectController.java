package ru.t1.dsinetsky.tm.api.controller;

public interface IProjectController {

    void create();

    void show();

    void clear();

    void showProjectById();

    void showProjectByIndex();

    void updateProjectById();

    void updateProjectByIndex();

    void removeProjectById();

    void removeProjectByIndex();

    void createTestProjects();

    void startProjectById();

    void startProjectByIndex();

    void completeProjectById();

    void completeProjectByIndex();

    void changeStatusById();

    void changeStatusByIndex();

}
