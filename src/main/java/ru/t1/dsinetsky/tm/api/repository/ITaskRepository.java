package ru.t1.dsinetsky.tm.api.repository;

import ru.t1.dsinetsky.tm.model.Task;

import java.util.List;

public interface ITaskRepository {

    Task createTask(Task task);

    void clearTasks();

    List<Task> returnAll();

    Task findById(String id);

    Task findByIndex(int index);

    Task removeById(String id);

    Task removeByIndex(int index);

    int getSize();
}
