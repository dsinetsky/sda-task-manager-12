package ru.t1.dsinetsky.tm.api.repository;

import ru.t1.dsinetsky.tm.model.Project;

import java.util.List;

public interface IProjectRepository {

    List<Project> returnAll();

    Project add(Project project);

    void clearAll();

    Project findById(String id);

    Project findByIndex(int index);

    Project removeById(String id);

    Project removeByIndex(int index);

    int getSize();
}
