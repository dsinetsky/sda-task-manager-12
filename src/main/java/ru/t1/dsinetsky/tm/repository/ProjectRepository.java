package ru.t1.dsinetsky.tm.repository;

import ru.t1.dsinetsky.tm.api.repository.IProjectRepository;
import ru.t1.dsinetsky.tm.model.Project;

import java.util.ArrayList;
import java.util.List;

public final class ProjectRepository implements IProjectRepository {

    private final List<Project> projects = new ArrayList<>();

    @Override
    public List<Project> returnAll() {
        return projects;
    }

    @Override
    public Project add(final Project project) {
        projects.add(project);
        return project;
    }

    @Override
    public void clearAll() {
        projects.clear();
    }

    @Override
    public Project findById(String id) {
        for (final Project proj : projects) {
            if (id.equals(proj.getId())) return proj;
        }
        return null;
    }

    @Override
    public Project findByIndex(int index) {
        if (index >= projects.size()) return null;
        return projects.get(index);
    }

    @Override
    public Project removeById(String id) {
        Project proj = findById(id);
        if (proj == null) return null;
        else projects.remove(proj);
        return proj;
    }

    @Override
    public Project removeByIndex(int index) {
        Project proj = findByIndex(index);
        if (proj == null) return null;
        else projects.remove(proj);
        return proj;
    }

    @Override
    public int getSize() {
        return projects.size();
    }
}
