package ru.t1.dsinetsky.tm.repository;

import ru.t1.dsinetsky.tm.api.repository.ICommandRepository;
import ru.t1.dsinetsky.tm.constant.ArgumentConst;
import ru.t1.dsinetsky.tm.constant.TerminalConst;
import ru.t1.dsinetsky.tm.model.Command;

public final class CommandRepository implements ICommandRepository {

    private final static Command VERSION = new Command(
            TerminalConst.CMD_VERSION,
            ArgumentConst.CMD_VERSION,
            "Shows program version"
    );

    private final static Command INFO = new Command(
            TerminalConst.CMD_INFO,
            ArgumentConst.CMD_INFO,
            "Shows system info"
    );

    private final static Command ABOUT = new Command(
            TerminalConst.CMD_ABOUT,
            ArgumentConst.CMD_ABOUT,
            "Shows information about developer"

    );

    private final static Command HELP = new Command(
            TerminalConst.CMD_HELP,
            ArgumentConst.CMD_HELP,
            "Shows this message"
    );

    private final static Command EXIT = new Command(
            TerminalConst.CMD_EXIT,
            null,
            "Exit application"
    );

    private final static Command ARG = new Command(
            TerminalConst.CMD_ARG,
            ArgumentConst.CMD_ARG,
            "Shows list of arguments"
    );

    private final static Command CMD = new Command(
            TerminalConst.CMD_CMD,
            ArgumentConst.CMD_CMD,
            "Shows list of commands"
    );

    private final static Command PRJ_CLEAR = new Command(
            TerminalConst.CMD_PROJECT_CLEAR,
            null,
            "Clear all projects"
    );

    private final static Command PRJ_CREATE = new Command(
            TerminalConst.CMD_PROJECT_CREATE,
            null,
            "Create new project"
    );

    private final static Command PRJ_LIST = new Command(
            TerminalConst.CMD_PROJECT_LIST,
            null,
            "Show available projects"
    );

    private final static Command TASK_LIST = new Command(
            TerminalConst.CMD_TASK_LIST,
            null,
            "Show available tasks"
    );

    private final static Command TASK_CREATE = new Command(
            TerminalConst.CMD_TASK_CREATE,
            null,
            "Create new task"
    );

    private final static Command TASK_CLEAR = new Command(
            TerminalConst.CMD_TASK_CLEAR,
            null,
            "Clear all tasks"
    );

    private final static Command FIND_PROJECT_BY_ID = new Command(
            TerminalConst.CMD_FIND_PROJECT_BY_ID,
            null,
            "Show project (if any) found by id"
    );

    private final static Command FIND_PROJECT_BY_INDEX = new Command(
            TerminalConst.CMD_FIND_PROJECT_BY_INDEX,
            null,
            "Shows project (if any) found by index"
    );

    private final static Command FIND_TASK_BY_ID = new Command(
            TerminalConst.CMD_FIND_TASK_BY_ID,
            null,
            "Show task (if any) found by id"
    );

    private final static Command FIND_TASK_BY_INDEX = new Command(
            TerminalConst.CMD_FIND_TASK_BY_INDEX,
            null,
            "Shows task (if any) found by index"
    );

    private final static Command UPDATE_PROJECT_BY_ID = new Command(
            TerminalConst.CMD_UPD_PROJECT_BY_ID,
            null,
            "Updates name and description of project (if any) found by id"
    );

    private final static Command UPDATE_PROJECT_BY_INDEX = new Command(
            TerminalConst.CMD_UPD_PROJECT_BY_INDEX,
            null,
            "Updates name and description of project (if any) found by index"
    );

    private final static Command UPDATE_TASK_BY_ID = new Command(
            TerminalConst.CMD_UPD_TASK_BY_ID,
            null,
            "Updates name and description of task (if any) found by id"
    );

    private final static Command UPDATE_TASK_BY_INDEX = new Command(
            TerminalConst.CMD_UPD_TASK_BY_INDEX,
            null,
            "Updates name and description of task (if any) found by index"
    );

    private final static Command REMOVE_PROJECT_BY_ID = new Command(
            TerminalConst.CMD_REMOVE_PROJECT_BY_ID,
            null,
            "Removes project (if any) found by id"
    );

    private final static Command REMOVE_PROJECT_BY_INDEX = new Command(
            TerminalConst.CMD_REMOVE_PROJECT_BY_INDEX,
            null,
            "Removes project (if any) found by index"
    );

    private final static Command REMOVE_TASK_BY_ID = new Command(
            TerminalConst.CMD_REMOVE_TASK_BY_ID,
            null,
            "Removes task (if any) found by id"
    );

    private final static Command REMOVE_TASK_BY_INDEX = new Command(
            TerminalConst.CMD_REMOVE_TASK_BY_INDEX,
            null,
            "Removes task (if any) found by index"
    );

    private final static Command PROJECT_START_BY_ID = new Command(
            TerminalConst.CMD_PROJECT_START_BY_ID,
            null,
            "Starts project (if any) found by id"
    );

    private final static Command PROJECT_START_BY_INDEX = new Command(
            TerminalConst.CMD_PROJECT_START_BY_INDEX,
            null,
            "Starts project (if any) found by index"
    );

    private final static Command PROJECT_COMPLETE_BY_ID = new Command(
            TerminalConst.CMD_PROJECT_COMPLETE_ID,
            null,
            "Completes project (if any) found by id"
    );

    private final static Command PROJECT_COMPLETE_BY_INDEX = new Command(
            TerminalConst.CMD_PROJECT_COMPLETE_INDEX,
            null,
            "Completes project (if any) found by index"
    );

    private final static Command PROJECT_CHANGE_STATUS_BY_ID = new Command(
            TerminalConst.CMD_PROJECT_CHANGE_STATUS_BY_ID,
            null,
            "Changes status of project (if any) found by id"
    );

    private final static Command PROJECT_CHANGE_STATUS_BY_INDEX = new Command(
            TerminalConst.CMD_PROJECT_CHANGE_STATUS_BY_INDEX,
            null,
            "Changes status of project (if any) found by index"
    );

    private final static Command TEST_PROJECTS = new Command(
            TerminalConst.CMD_CREATE_TEST_PROJECTS,
            null,
            "Create 10 projects for tests"
    );

    private final static Command TEST_TASKS = new Command(
            TerminalConst.CMD_CREATE_TEST_TASKS,
            null,
            "Create 10 tasks for tests"
    );

    private final static Command TASK_START_BY_ID = new Command(
            TerminalConst.CMD_TASK_START_BY_ID,
            null,
            "Starts task (if any) found by id"
    );

    private final static Command TASK_START_BY_INDEX = new Command(
            TerminalConst.CMD_TASK_START_BY_INDEX,
            null,
            "Starts task (if any) found by index"
    );

    private final static Command TASK_COMPLETE_BY_ID = new Command(
            TerminalConst.CMD_TASK_COMPLETE_ID,
            null,
            "Completes task (if any) found by id"
    );

    private final static Command TASK_COMPLETE_BY_INDEX = new Command(
            TerminalConst.CMD_TASK_COMPLETE_INDEX,
            null,
            "Completes task (if any) found by index"
    );

    private final static Command TASK_CHANGE_STATUS_BY_ID = new Command(
            TerminalConst.CMD_TASK_CHANGE_STATUS_BY_ID,
            null,
            "Changes status of task (if any) found by id"
    );

    private final static Command TASK_CHANGE_STATUS_BY_INDEX = new Command(
            TerminalConst.CMD_TASK_CHANGE_STATUS_BY_INDEX,
            null,
            "Changes status of task (if any) found by index"
    );

    private final static Command[] TERMINAL_COMMANDS = new Command[]{
            HELP, VERSION, ABOUT, INFO, ARG, CMD,

            PRJ_CLEAR, PRJ_CREATE, PRJ_LIST, TEST_PROJECTS,
            FIND_PROJECT_BY_ID, FIND_PROJECT_BY_INDEX,
            UPDATE_PROJECT_BY_ID, UPDATE_PROJECT_BY_INDEX,
            REMOVE_PROJECT_BY_ID, REMOVE_PROJECT_BY_INDEX,
            PROJECT_START_BY_ID, PROJECT_START_BY_INDEX,
            PROJECT_COMPLETE_BY_ID, PROJECT_COMPLETE_BY_INDEX,
            PROJECT_CHANGE_STATUS_BY_ID, PROJECT_CHANGE_STATUS_BY_INDEX,

            TASK_CLEAR, TASK_CREATE, TASK_LIST, TEST_TASKS,
            FIND_TASK_BY_ID, FIND_TASK_BY_INDEX,
            UPDATE_TASK_BY_ID, UPDATE_TASK_BY_INDEX,
            REMOVE_TASK_BY_ID, REMOVE_TASK_BY_INDEX,
            TASK_START_BY_ID, TASK_START_BY_INDEX,
            TASK_COMPLETE_BY_ID, TASK_COMPLETE_BY_INDEX,
            TASK_CHANGE_STATUS_BY_ID, TASK_CHANGE_STATUS_BY_INDEX,

            EXIT
    };

    @Override
    public Command[] getTerminalCommands() {
        return TERMINAL_COMMANDS;
    }
}
