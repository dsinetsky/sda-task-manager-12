package ru.t1.dsinetsky.tm.controller;

import ru.t1.dsinetsky.tm.api.controller.IProjectController;
import ru.t1.dsinetsky.tm.api.service.IProjectService;
import ru.t1.dsinetsky.tm.enumerated.Status;
import ru.t1.dsinetsky.tm.model.Project;
import ru.t1.dsinetsky.tm.util.TerminalUtil;

import java.util.List;

public final class ProjectController implements IProjectController {

    private final IProjectService projectService;

    public ProjectController(final IProjectService projectService) {
        this.projectService = projectService;
    }

    private void showProject(Project project) {
        System.out.println("Id: " + project.getId());
        System.out.println("Name: " + project.getName());
        System.out.println("Desc: " + project.getDesc());
        System.out.println("Status: " + Status.toName(project.getStatus()));
    }

    @Override
    public void create() {
        System.out.println("Enter name of new project:");
        final String name = TerminalUtil.nextLine();
        System.out.println("Enter description:");
        final String desc = TerminalUtil.nextLine();
        Project proj = projectService.create(name, desc);
        if (proj == null) System.out.println("Error! Project cant be created");
        else System.out.println("Project successfully created!");
    }

    @Override
    public void show() {
        System.out.println("List of projects:");
        List<Project> listProjects = projectService.returnAll();
        int index = 1;
        for (Project proj : listProjects) {
            System.out.println(index + ". " + proj.toString());
            index++;
        }
    }

    @Override
    public void clear() {
        projectService.clearAll();
        System.out.println("All projects successfully cleared!");
    }

    @Override
    public void showProjectById() {
        System.out.println("Enter id of project:");
        String id = TerminalUtil.nextLine();
        Project proj = projectService.findById(id);
        if (proj == null)
            System.out.println("Project not found!");
        else
            showProject(proj);
    }

    @Override
    public void showProjectByIndex() {
        System.out.println("Enter index of project:");
        int index = TerminalUtil.nextInt() - 1;
        Project proj = projectService.findByIndex(index);
        if (proj == null)
            System.out.println("Project not found!");
        else
            showProject(proj);
    }

    @Override
    public void updateProjectById() {
        System.out.println("Enter id of project:");
        String id = TerminalUtil.nextLine();
        Project proj = projectService.findById(id);
        if (proj == null)
            System.out.println("Project not found");
        else {
            System.out.println("Enter new name:");
            String name = TerminalUtil.nextLine();
            System.out.println("Enter new description:");
            String desc = TerminalUtil.nextLine();
            proj = projectService.updateById(id, name, desc);
            showProject(proj);
            System.out.println("Project successfully updated!");
        }
    }

    @Override
    public void updateProjectByIndex() {
        System.out.println("Enter index of project:");
        int index = TerminalUtil.nextInt() - 1;
        Project proj = projectService.findByIndex(index);
        if (proj == null)
            System.out.println("Project not found");
        else {
            System.out.println("Enter new name:");
            String name = TerminalUtil.nextLine();
            System.out.println("Enter new description:");
            String desc = TerminalUtil.nextLine();
            proj = projectService.updateByIndex(index, name, desc);
            showProject(proj);
            System.out.println("Project successfully updated!");
        }
    }

    @Override
    public void removeProjectById() {
        System.out.println("Enter id of project:");
        String id = TerminalUtil.nextLine();
        Project proj = projectService.removeById(id);
        if (proj == null)
            System.out.println("Project not found!");
        else
            System.out.println("Project successfully removed!");
    }

    @Override
    public void removeProjectByIndex() {
        System.out.println("Enter index of project:");
        int index = TerminalUtil.nextInt() - 1;
        Project proj = projectService.removeByIndex(index);
        if (proj == null)
            System.out.println("Project not found!");
        else
            System.out.println("Project successfully removed!");
    }

    @Override
    public void createTestProjects() {
        projectService.createTestProjects();
        System.out.println("10 test projects created!");
    }

    @Override
    public void startProjectById() {
        System.out.println("Enter id of project:");
        String id = TerminalUtil.nextLine();
        Project proj = projectService.changeStatusById(id, Status.IN_PROGRESS);
        if (proj == null)
            System.out.println("Project not found!");
        else {
            showProject(proj);
            System.out.println("Project successfully started!");
        }
    }

    @Override
    public void startProjectByIndex() {
        System.out.println("Enter index of project:");
        int index = TerminalUtil.nextInt() - 1;
        Project proj = projectService.changeStatusByIndex(index, Status.IN_PROGRESS);
        if (proj == null)
            System.out.println("Project not found!");
        else {
            showProject(proj);
            System.out.println("Project successfully started!");
        }
    }

    @Override
    public void completeProjectById() {
        System.out.println("Enter id of project:");
        String id = TerminalUtil.nextLine();
        Project proj = projectService.changeStatusById(id, Status.COMPLETED);
        if (proj == null)
            System.out.println("Project not found!");
        else {
            showProject(proj);
            System.out.println("Project successfully completed!");
        }
    }

    @Override
    public void completeProjectByIndex() {
        System.out.println("Enter index of project:");
        int index = TerminalUtil.nextInt() - 1;
        Project proj = projectService.changeStatusByIndex(index, Status.COMPLETED);
        if (proj == null)
            System.out.println("Project not found!");
        else {
            showProject(proj);
            System.out.println("Project successfully completed!");
        }
    }

    @Override
    public void changeStatusById() {
        System.out.println("Enter id of project:");
        String id = TerminalUtil.nextLine();
        System.out.println("Enter new status. Available statuses:");
        System.out.println(Status.getStatuses());
        String newStatus = TerminalUtil.nextLine();
        Status status = Status.toStatus(newStatus);
        Project proj = projectService.changeStatusById(id, status);
        if (proj == null)
            System.out.println("Project not found!");
        else {
            showProject(proj);
            System.out.println("Status successfully changed");
        }
    }

    @Override
    public void changeStatusByIndex() {
        System.out.println("Enter index of project:");
        int index = TerminalUtil.nextInt() - 1;
        System.out.println("Enter new status. Available statuses:");
        System.out.println(Status.getStatuses());
        String newStatus = TerminalUtil.nextLine();
        Status status = Status.toStatus(newStatus);
        Project proj = projectService.changeStatusByIndex(index, status);
        if (proj == null)
            System.out.println("Project not found!");
        else {
            showProject(proj);
            System.out.println("Status successfully changed");
        }
    }

}
