package ru.t1.dsinetsky.tm.service;

import ru.t1.dsinetsky.tm.api.repository.ITaskRepository;
import ru.t1.dsinetsky.tm.api.service.ITaskService;
import ru.t1.dsinetsky.tm.enumerated.Status;
import ru.t1.dsinetsky.tm.model.Task;

import java.util.List;

public class TaskService implements ITaskService {

    private ITaskRepository taskRepository;

    public TaskService(ITaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    @Override
    public void clearAll() {
        taskRepository.clearTasks();
    }

    @Override
    public List<Task> returnAll() {
        return taskRepository.returnAll();
    }

    @Override
    public Task create(String name) {
        if (name == null || name.isEmpty()) return null;
        return taskRepository.createTask(new Task(name));
    }

    @Override
    public Task create(String name, String desc) {
        if (name == null || name.isEmpty()) return null;
        if (desc == null || desc.isEmpty()) return null;
        return taskRepository.createTask(new Task(name, desc));
    }

    @Override
    public Task findById(String id) {
        if (id != null && !id.isEmpty()) return taskRepository.findById(id);
        return null;
    }

    @Override
    public Task findByIndex(int index) {
        if (index < 0) return null;
        return taskRepository.findByIndex(index);
    }

    @Override
    public Task removeById(String id) {
        if (id != null && !id.isEmpty()) return taskRepository.removeById(id);
        return null;
    }

    @Override
    public Task removeByIndex(int index) {
        if (index < 0) return null;
        return taskRepository.removeByIndex(index);
    }

    @Override
    public Task updateById(String id, String name, String desc) {
        if (id == null || id.isEmpty()) return null;
        if (name == null || name.isEmpty()) return null;
        Task task = findById(id);
        if (task == null) return null;
        task.setName(name);
        task.setDesc(desc);
        return task;
    }

    @Override
    public Task updateByIndex(int index, String name, String desc) {
        if (index < 0) return null;
        if (name == null || name.isEmpty()) return null;
        Task task = findByIndex(index);
        if (task == null) return null;
        task.setName(name);
        task.setDesc(desc);
        return task;
    }

    @Override
    public Task changeStatusById(String id, Status status) {
        if (id == null || id.isEmpty()) return null;
        Task task = findById(id);
        if (task == null) return null;
        task.setStatus(status);
        return task;
    }

    @Override
    public Task changeStatusByIndex(int index, Status status) {
        if (index < 0) return null;
        if (index >= taskRepository.getSize()) return null;
        Task task = findByIndex(index);
        if (task == null) return null;
        task.setStatus(status);
        return task;
    }

    @Override
    public void createTestTasks() {
        String name = "task";
        String desc = "desc";
        for (int i = 1; i < 11; i++) {
            create(name + i, desc + i);
        }
    }

}
