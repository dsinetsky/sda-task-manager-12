package ru.t1.dsinetsky.tm.model;

import ru.t1.dsinetsky.tm.enumerated.Status;

import java.util.UUID;

public final class Task {

    private String id = UUID.randomUUID().toString();

    private String name = "";

    private String desc = "";

    private Status status = Status.NOT_STARTED;

    public Task() {
    }

    public Task(String name) {
        this.name = name;
    }

    public Task(String name, String desc) {
        this.name = name;
        this.desc = desc;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return new StringBuilder().append(name).append(" : ").append(desc).toString();
    }
}
