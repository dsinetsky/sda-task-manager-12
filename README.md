# SDA-TASK-MANAGER-12

## DEVELOPER

**NAME:** Dmitry Sinetsky

**E-mail:** dsinetsky@t1-consulting.ru
## SOFTWARE

**JAVA:** JDK 1.8

## HARDWARE

**CPU:** i7

**RAM:** 16Gb

**HDD:** 250Gb

## LAUNCH


```Shell
java -jar .\sda-task-manager-12.jar
```

## APPLICATION BUILD
```
mvn clean install
```

